use terminal_size::{Width, terminal_size};
use std::io::Write;
use dirs;
use std::env::consts::OS;

pub fn check_list(input: String) {
    let api = input.trim();
    let size = terminal_size();

    if let Some((Width(w), _)) = size {
        let mut i = 1;
        while i < w {
            print!("=");
            i = i + 1;
        }
        print!("\n");
    } else {
        println!("Unable to get terminal size");
    }
    
    let conf_check = dirs::config_dir().unwrap()
        .join("Weather-Man/config.toml").exists();
    
    if conf_check == true{
        println!("Bootstrap Report: Config File exists");
    }
    else{
        create_config(api);
        std::process::exit(1);
    }  
}

fn create_config(api_key: &str) {
    println!("Bootstrap Report: Config File does not exist");

    let create_dir = dirs::config_dir()
        .expect("Failure to find config directory :(")
        .join("Weather-Man");

    std::fs::create_dir_all(&create_dir).unwrap();
    let conf_file_format = format!("[api]\nkey = \"{}\"", api_key);
    let conf_file = create_dir.join("config.toml");

    std::fs::File::create(conf_file)
        .expect("Could not create Config File")
        .write_all(conf_file_format.as_bytes())
        .unwrap();

    println!("Created config.toml File, Please register an API at: https://developer.accuweather.com/");

    if OS == "windows" {
        println!("Config Directory is Located at \"{}\\Weather-Man\\config.toml\"", dirs::config_dir()
            .expect("Failure to find config directory :(")
            .display());
    } else {
        println!("Config Directory is Located at \"{}/Weather-Man/config.toml\"", dirs::config_dir()
            .expect("Failure to find config directory :(")
            .display());
    }
}