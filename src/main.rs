mod weather;
mod bootstrap;
mod tui;

use std::io;
use weather::{fetch_forecast, locate_city};
use bootstrap::check_list;
use tui::ui;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("Enter an API Key (First Time Thing Only): ");
    let mut apikey = String::new();
    io::stdin().read_line(&mut apikey)?;
    check_list(apikey);
    println!("Enter a city name:");
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;

    let location_key = locate_city(input).await?;
    println!("\nLocation Code: {}", location_key);
    let forecast_data = fetch_forecast(&location_key).await?;

    for forecastdata in forecast_data.daily_forecasts {
        println!(
            "Date: {}\nMinimum Temperature: {} {}\nMaximum Temperature: {} {}\n",
            forecastdata.date,
            forecastdata.temperature.minimum.value,
            forecastdata.temperature.minimum.unit,
            forecastdata.temperature.maximum.value,
            forecastdata.temperature.maximum.unit,
        );
    }
    ui();
    Ok(())
}

