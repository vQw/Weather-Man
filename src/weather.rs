use reqwest;
use serde::Deserialize;
use std::error::Error;
use toml::from_str;
use dirs;

pub async fn locate_city(text: String) -> Result<String, Box<dyn Error>> {
    let input = text.trim();
    let api_key = fetch_api().await?;
    let key = api_key.to_string();    
    let base_url = "http://dataservice.accuweather.com/locations/v1/cities/search";

    let query_url = format!("{}?apikey={}&q={}", base_url, key, input);
    
    let response = reqwest::get(&query_url).await?;
    let body = response.text().await?;
    let location: Vec<Location> = serde_json::from_str(&body)?;

    if let Some(location) = location.first() {
        let string = location.key.to_string();
        Ok(string)
    } else {
        Err("No location found".into())
    }
}

pub async fn fetch_forecast(location_key: &str) -> Result<Forecast, Box<dyn Error>> {
    let api_key = fetch_api().await?;
    let key = api_key.to_string();    
    let url = format!(
        "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}?apikey={}",
        location_key, key
    );

    let response = reqwest::get(&url).await?;
    let body = response.text().await?;

    let data: Forecast = serde_json::from_str(&body)?;

    Ok(data)
}

#[derive(Debug, Deserialize)]
pub struct Location {
    #[serde(rename = "Key")]
    pub key: String,
}

#[derive(Debug, Deserialize)]
pub struct TemperatureUnit {
    #[serde(rename = "Value")]
    pub value: f32,
    #[serde(rename = "Unit")]
    pub unit: String,
}

#[derive(Debug, Deserialize)]
pub struct DailyForecast {
    #[serde(rename = "Date")]
    pub date: String,
    #[serde(rename = "Temperature")]
    pub temperature: Temperature,
}

#[derive(Debug, Deserialize)]
pub struct Temperature {
    #[serde(rename = "Minimum")]
    pub minimum: TemperatureUnit,
    #[serde(rename = "Maximum")]
    pub maximum: TemperatureUnit,
}

#[derive(Debug, Deserialize)]
pub struct Forecast {
    #[serde(rename = "DailyForecasts")]
    pub daily_forecasts: Vec<DailyForecast>,
}

#[derive(Debug, Deserialize)]
struct Config {
    api: Api,
}

#[derive(Debug, Deserialize)]
struct Api {
    key: String,
}

pub async fn fetch_api() -> Result<String, Box<dyn std::error::Error>> {
    let mut config_path = dirs::config_dir().unwrap();
    config_path.push("Weather-Man/config.toml");
    let config_str = std::fs::read_to_string(config_path).unwrap();
    let config: Config = from_str(&config_str).unwrap();
    let api_key = config.api.key;
    return Ok(api_key);
}
