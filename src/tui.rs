use std::io::{stdout, Write};
use tui::backend::CrosstermBackend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::{Color, Style};
use tui::widgets::{Block, Borders, Paragraph, Text};
use tui::Terminal;

pub fn ui() -> Result<(), Box<dyn std::error::Error>> {
    // Create a terminal backend and terminal instance
    let backend = CrosstermBackend::new(stdout());
    let mut terminal = Terminal::new(backend)?;

    // Define some variables to display
    let var1 = "Hello";
    let var2 = "World";
    let var3 = 42;

    // Define a layout for the output
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Percentage(50),
            Constraint::Percentage(25),
            Constraint::Percentage(25),
        ])
        .margin(5);

    // Create a text widget for each variable
    let var1_text = Text::styled(var1, Style::default().fg(Color::Green));
    let var2_text = Text::styled(var2, Style::default().fg(Color::Yellow));
    let var3_text = Text::styled(format!("{}", var3), Style::default().fg(Color::Blue));

    // Create a paragraph widget with the text widgets
    let paragraph = Paragraph::new(vec![var1_text, var2_text, var3_text])
        .block(Block::default().borders(Borders::ALL).title("My Variables"))
        .alignment(Alignment::Left);

    // Render the output to the terminal
    terminal.clear()?;
    terminal.draw(|f| {
        let size = f.size();
        let chunks = layout.split(size);
        f.render_widget(paragraph, chunks[0]);
    })?;
    Ok(())
}

